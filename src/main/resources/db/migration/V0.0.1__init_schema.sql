create table account
(
    id           uuid,
    username     text not null,
    email        text not null,
    display_name text,
    avatar_url   text,

    constraint pk_user primary key (id),
    constraint u_username unique (username),
    constraint u_email unique (email)
);

create table post
(
    id        uuid,
    title     text      not null,
    "text"    text      not null,
    category  text      not null,
    create_at timestamp not null,
    update_at timestamp not null,
    author_id uuid      not null,

    constraint pk_post primary key (id),
    constraint fk_post_author foreign key (author_id)
        references account (id)
);