package tech.jpaz.network.posts.requests;

import jakarta.validation.constraints.NotEmpty;

public record PostRequest(
        @NotEmpty
        String title,
        @NotEmpty
        String text,
        @NotEmpty
        String category,
        @NotEmpty
        AccountRequest author
) {
}
