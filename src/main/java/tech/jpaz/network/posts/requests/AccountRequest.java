package tech.jpaz.network.posts.requests;

import jakarta.validation.constraints.NotEmpty;

public record AccountRequest(@NotEmpty String username) {
}
