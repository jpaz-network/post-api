package tech.jpaz.network.posts.responses;

import tech.jpaz.network.posts.entities.Post;

import java.util.UUID;

public record PostResponse(UUID id,
                           String title,
                           String text,
                           String category,
                           AccountResponse author
) {

    public PostResponse(Post post) {
        this(
                post.getId(),
                post.getTitle(),
                post.getText(),
                post.getCategory(),
                new AccountResponse(post.getAuthor())
        );
    }
}
