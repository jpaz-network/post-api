package tech.jpaz.network.posts.responses;

import tech.jpaz.network.posts.entities.Account;

public record AccountResponse(
        String username,
        String email,
        String avatarUrl
) {

    public AccountResponse(Account account) {
        this(
                account.getUsername(),
                account.getEmail(),
                account.getAvatarUrl()
        );
    }
}
