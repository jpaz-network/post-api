package tech.jpaz.network.posts.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import tech.jpaz.network.broker.messages.AccountCreateMessage;
import tech.jpaz.network.posts.services.AccountService;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccountLister {

    private final AccountService accountService;

    @JmsListener(destination = "account.create")
    public void onCreate(AccountCreateMessage accountCreateMessage) {
        log.info("saving account " + accountCreateMessage);

        this.accountService.create(accountCreateMessage);
    }

}
