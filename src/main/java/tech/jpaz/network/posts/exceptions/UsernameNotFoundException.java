package tech.jpaz.network.posts.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import tech.jpaz.network.posts.requests.AccountRequest;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UsernameNotFoundException extends RuntimeException {

    public UsernameNotFoundException(String username) {
        super("account with username \"%s\" not found".formatted(username));
    }

    public static UsernameNotFoundException from(AccountRequest accountRequest) {
        return new UsernameNotFoundException(accountRequest.username());
    }
}
