package tech.jpaz.network.posts.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tech.jpaz.network.broker.messages.AccountCreateMessage;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Account {

    @Id
    private UUID id;

    @NotEmpty
    private String username;

    @NotEmpty
    @Email
    private String email;

    private String displayName;

    private String avatarUrl;

    public Account(AccountCreateMessage accountCreateMessage) {
        this.id = accountCreateMessage.id();
        this.username = accountCreateMessage.username();
        this.email = accountCreateMessage.email();
        this.displayName = accountCreateMessage.displayName();
        this.avatarUrl = accountCreateMessage.avatarUrl();
    }
}
