package tech.jpaz.network.posts.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tech.jpaz.network.posts.requests.PostRequest;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @NotEmpty
    private String title;

    @NotEmpty
    private String text;

    @NotEmpty
    private String category;

    @NotNull
    private LocalDateTime createAt;

    @NotNull
    private LocalDateTime updateAt;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private Account author;

    public Post(PostRequest postRequest, Account author) {
        this.title = postRequest.title();
        this.text = postRequest.text();
        this.category = postRequest.category();
        var now = LocalDateTime.now();
        this.createAt = now;
        this.updateAt = now;
        this.author = author;
    }
}
