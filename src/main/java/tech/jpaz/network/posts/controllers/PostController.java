package tech.jpaz.network.posts.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import tech.jpaz.network.posts.requests.PostRequest;
import tech.jpaz.network.posts.responses.PostResponse;
import tech.jpaz.network.posts.services.PostService;

import java.util.List;
import java.util.UUID;

@Slf4j
@Controller
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @MutationMapping
    public PostResponse createPost(@Argument PostRequest request) {
        log.info("createPost(request: %s)".formatted(request));

        return this.postService.create(request);
    }

    @QueryMapping
    public List<PostResponse> recentPosts(@Argument Integer count, @Argument Integer offset) {
        log.info("recentPosts(count: %d, offset: %d)".formatted(count, offset));

        return this.postService.recentPosts(count, offset);
    }

    @QueryMapping
    public List<PostResponse> recentPostsByAuthor(@Argument UUID authorId, @Argument Integer count, @Argument Integer offset) {
        log.info("recentPostsByAuthor(authorId: %s, count: %d, offset: %d)".formatted(authorId, count, offset));

        return this.postService.recentPostsByAuthor(authorId, count, offset);
    }
}
