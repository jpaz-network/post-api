package tech.jpaz.network.posts.producers;

import tech.jpaz.network.posts.entities.Post;

public interface PostProducer {

    void onCreatePost(Post post);

}
