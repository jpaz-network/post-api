package tech.jpaz.network.posts.producers.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import tech.jpaz.network.broker.messages.PostCreateMessage;
import tech.jpaz.network.posts.entities.Post;
import tech.jpaz.network.posts.producers.PostProducer;

@Slf4j
@Component
@RequiredArgsConstructor
public class PostProducerImpl implements PostProducer {

    private final JmsTemplate jmsTemplate;

    @Override
    public void onCreatePost(Post post) {
        var postCreateMessage = new PostCreateMessage(
                post.getId(),
                post.getTitle(),
                post.getText(),
                post.getAuthor().getId()
        );

        log.info("sending post create message: " + postCreateMessage);

        jmsTemplate.convertAndSend("post.create", postCreateMessage);
    }
}
