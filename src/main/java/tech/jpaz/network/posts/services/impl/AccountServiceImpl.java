package tech.jpaz.network.posts.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tech.jpaz.network.broker.messages.AccountCreateMessage;
import tech.jpaz.network.posts.entities.Account;
import tech.jpaz.network.posts.repositories.AccountRepository;
import tech.jpaz.network.posts.responses.AccountResponse;
import tech.jpaz.network.posts.services.AccountService;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public AccountResponse create(AccountCreateMessage accountCreateMessage) {
        var account = this.accountRepository.save(new Account(accountCreateMessage));

        return new AccountResponse(account);
    }

}
