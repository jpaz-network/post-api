package tech.jpaz.network.posts.services.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.jpaz.network.posts.entities.Post;
import tech.jpaz.network.posts.exceptions.UsernameNotFoundException;
import tech.jpaz.network.posts.producers.PostProducer;
import tech.jpaz.network.posts.repositories.AccountRepository;
import tech.jpaz.network.posts.repositories.PostRepository;
import tech.jpaz.network.posts.requests.PostRequest;
import tech.jpaz.network.posts.responses.PostResponse;
import tech.jpaz.network.posts.services.PostService;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostProducer postProducer;
    private final PostRepository postRepository;
    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public PostResponse create(PostRequest request) {
        var author = this.accountRepository.findByUsername(request.author().username())
                .orElseThrow(() -> UsernameNotFoundException.from(request.author()));

        var post = this.postRepository.save(new Post(request, author));

        postProducer.onCreatePost(post);

        return new PostResponse(post);
    }

    @Override
    @Transactional
    public List<PostResponse> recentPosts(Integer count, Integer offset) {
        return this.postRepository.recentPosts(count, offset)
                .stream().map(PostResponse::new).toList();
    }

    @Override
    @Transactional
    public List<PostResponse> recentPostsByAuthor(UUID authorId, Integer count, Integer offset) {
        return this.postRepository.recentPostsByAuthor(authorId, count, offset)
                .stream().map(PostResponse::new).toList();
    }
}
