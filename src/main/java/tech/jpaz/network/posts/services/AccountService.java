package tech.jpaz.network.posts.services;

import tech.jpaz.network.broker.messages.AccountCreateMessage;
import tech.jpaz.network.posts.responses.AccountResponse;

public interface AccountService {

    AccountResponse create(AccountCreateMessage accountCreateMessage);

}
