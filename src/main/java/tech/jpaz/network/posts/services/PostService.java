package tech.jpaz.network.posts.services;

import tech.jpaz.network.posts.requests.PostRequest;
import tech.jpaz.network.posts.responses.PostResponse;

import java.util.List;
import java.util.UUID;

public interface PostService {

    PostResponse create(PostRequest request);

    List<PostResponse> recentPosts(Integer count, Integer offset);

    List<PostResponse> recentPostsByAuthor(UUID authorId, Integer count, Integer offset);
}
