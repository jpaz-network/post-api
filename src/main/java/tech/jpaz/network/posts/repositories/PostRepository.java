package tech.jpaz.network.posts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tech.jpaz.network.posts.entities.Post;

import java.util.List;
import java.util.UUID;

@Repository
public interface PostRepository extends JpaRepository<Post, UUID> {

    @Query(value = "select p.* from post p order by p.update_at desc offset :offset limit :count", nativeQuery = true)
    List<Post> recentPosts(@Param("count") Integer count, @Param("offset") Integer offset);

    @Query(value = "select p.* from post p where p.author_id = :authorId " +
            "order by p.update_at desc offset :offset limit :count", nativeQuery = true)
    List<Post> recentPostsByAuthor(@Param("authorId") UUID authorId, @Param("count") Integer count, @Param("offset") Integer offset);
}
